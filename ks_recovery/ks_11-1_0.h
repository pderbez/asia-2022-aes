#include <stdint.h>

typedef struct position10_2
{
	uint8_t a;
	uint8_t b;
}POSITION10_2,*Position10_2;

typedef struct chaine10_2
{
	unsigned char d0;
	unsigned char d1;
	uint8_t a;
	uint8_t b;
}CHAINE10_2,*Chaine10_2;

#define taille1_10 0x100L
#define taille2_10 0x100L
typedef struct position13_2
{
	uint8_t a;
	uint8_t b;
}POSITION13_2,*Position13_2;

typedef struct chaine13_2
{
	unsigned char d0;
	unsigned char d1;
	uint8_t a;
	uint8_t b;
}CHAINE13_2,*Chaine13_2;

#define taille1_13 0x100L
#define taille2_13 0x100L
