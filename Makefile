# Generic Makefile for compiling a simple executable.

#Polynomial used to define the finite field GF(2^n) -
# AES Field
GF_POLY := 0x11b
# F_2
#GF_POLY := 0x02
# Piccolo
#GF_POLY := 0x13

CC := g++
#CC := icpc
SRCDIR := src
BUILDDIR := build
GRBPATH = /opt/gurobi
USERDEFINES := -DGF_POLY=$(GF_POLY)
#CFLAGS := -fast -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost
#CFLAGS := -fast -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost -prof-use -prof-dir=profile/
#CFLAGS := -fast -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -opt-multi-version-aggressive -xHost -prof-gen -prof-dir=profile/
CFLAGS := -O3 -s -mtune=native -march=native -fopenmp -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O3 -s -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O3 -march=native -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O2 -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG
#CFLAGS := -O2 -pg -g -Wall -Wno-unused-variable -std=c++11 -DNDEBUG

#CFLAGS := -O3 -s -march=core2 -Wall -Wno-unused-variable -std=c++11 -DNDEBUG -static

LIBS := -L$(GRBPATH)/linux64/lib -lgurobi_c++ -lgurobi95 -ltbb -lm

TARGET := Boom_$(GF_POLY)

SOURCES := $(shell find $(SRCDIR) -type f -name *.cpp)
HEADERS := $(shell find $(SRCDIR) -type f -name *.hpp)



#OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.cpp=.o))

#DEPS := $(OBJECTS:.o=.deps)
DEPS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.cpp=.deps))


all: $(TARGET)

$(TARGET): $(OBJECTS)
	@echo " Linking GDSSearch..."; $(CC) $(USERDEFINES) $(CFLAGS) $^ $(LIBS) -o $(TARGET)



$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp  $(HEADERS)
	@mkdir -p $(BUILDDIR)
	@echo " CC $<"; $(CC) $(USERDEFINES) $(CFLAGS) -MD -MF $(@:.o=.deps) -c -o $@ $<

clean:
	@echo " Cleaning..."; $(RM) -r $(BUILDDIR) $(TARGET) *~

-include $(DEPS)

$(BUILDDIR)/GFElement.o:   .FORCE

.FORCE:

.PHONY: clean .FORCE
