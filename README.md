# Asia2022_AES

This repository contains the MILP model associated to the paper "Revisiting Related-Key Boomerang attacks on AES using computer-aided tool" published at ASIACRYPT 2022.

The directory "ks_recovery" contains the procedure to retrieve the missing key bytes at the end of the boomerang attack. It was obtained using the tool from the paper "Automatic Search of Attacks on Round-Reduced AES and Applications" published at CRYPTO 2011.

