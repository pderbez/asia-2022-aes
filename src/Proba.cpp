#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <cstdint>
#include <iostream>

using namespace std;

unsigned ithbit(unsigned x, unsigned i) {return ((x >> i) & 1);}

map<unsigned, double> proba() {
  auto sbox = vector<uint8_t>(
      {0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b,
       0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0,
       0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26,
       0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
       0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2,
       0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0,
       0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed,
       0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
       0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f,
       0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5,
       0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec,
       0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
       0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14,
       0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c,
       0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d,
       0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
       0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f,
       0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e,
       0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11,
       0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
       0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f,
       0xb0, 0x54, 0xbb, 0x16});
  vector<uint8_t> rsbox (256);
  for (unsigned x = 0; x < 256; ++x) rsbox[sbox[x]] = x;

  unsigned cpt = 0;

  map<unsigned, double> mapproba;
  set<double> myproba;

  #pragma omp parallel for schedule(dynamic)
  for (unsigned conf = 0; conf < 1u << 12; ++conf) {
    bool flag = true;
    for (unsigned i = 0; i < 4; ++i) flag = flag && (ithbit(conf, 3*i) <= ithbit(conf, 3*i+1)) && (ithbit(conf, 3*i+1) <= ithbit(conf, 3*i+2));
    flag = flag && (ithbit(conf, 2) >= ithbit(conf, 5));
    flag = flag && (ithbit(conf, 8) <= ithbit(conf, 11));
    flag = flag && (ithbit(conf, 2) + ithbit(conf, 8) >= 1);
    flag = flag && (ithbit(conf, 5) + ithbit(conf, 11) >= 1);
    flag = flag && (ithbit(conf, 0) == ithbit(conf, 3));
    flag = flag && (ithbit(conf, 6) == ithbit(conf, 9));
    if (!flag) continue;

    double p1 = 0.0;
    double nmess = 256.0*256.0*256.0*256.0;
    if (ithbit(conf, 1) == 1) nmess /= 256.0;
    if (ithbit(conf, 10) == 1) nmess /= 256.0*256.0;
    else if (ithbit(conf, 4) == 1) nmess /= 256.0;

    if (ithbit(conf, 1) == 0 && ithbit(conf, 4) == 0 && ithbit(conf, 7) == 0 && ithbit(conf, 10) == 0) p1 = nmess;
    else if (ithbit(conf, 1) == 1) {
      for (unsigned d = ((ithbit(conf, 0) == 1) ? 0 : 1); d < ((ithbit(conf, 0) == 1) ? 1 : 256); ++d) {
        map<unsigned, double> tmp;
        for (unsigned x0 = 0; x0 < 256; ++x0) {
          unsigned x1 = x0 ^ d;
          unsigned y0 = sbox[x0], y1 = sbox[x1];
          for (unsigned x2 = 0; x2 < 256; ++x2) {
            unsigned y2 = sbox[x2];
            for (unsigned x3 = 0; x3 < 256; ++x3) {
              unsigned y3 = sbox[x3];
              unsigned z = 0;
              if ((x2 ^ x3) != (x0 ^ x1)) continue;
              if (ithbit(conf, 4) == 1) {
                if ((y0 ^ y1) != (y2 ^ y3)) continue;
                else z = (z << 8) | (y0 ^ y1);
              }
              if (ithbit(conf, 7) == 1) {
                if ((x0 ^ x2) != (x1 ^ x3)) continue;
                if (ithbit(conf, 6) == 1 && x0 != x2) continue;
                if (ithbit(conf, 6) == 0 && x0 == x2) continue;
                else z = (z << 8) | (x0 ^ x2);
              }
              if (ithbit(conf, 10) == 1) {
                if ((y0 ^ y2) != (y1 ^ y3)) continue;
                if (ithbit(conf, 9) == 1 && y0 != y2) continue;
                if (ithbit(conf, 9) == 0 && y0 == y2) continue;
                else z = (z << 8) | (y0 ^ y2);
              }
              tmp[z] += 1.0;
            }
          }
        }
        for (auto const & p : tmp) p1 = max(p1, p.second);
      }
    }
    else if (ithbit(conf, 4) == 1) {
      for (unsigned d = ((ithbit(conf, 3) == 1) ? 0 : 1); d < ((ithbit(conf, 3) == 1) ? 1 : 256); ++d) {
        map<unsigned, double> tmp;
        for (unsigned x0 = 0; x0 < 256; ++x0) {
          unsigned y0 = sbox[x0];
          unsigned y1 = y0 ^ d;
          unsigned x1 = rsbox[y1];
          for (unsigned x2 = 0; x2 < 256; ++x2) {
            unsigned y2 = sbox[x2];
            for (unsigned x3 = 0; x3 < 256; ++x3) {
              unsigned y3 = sbox[x3];
              unsigned z = 0;
              if ((y0 ^ y1) != (y2 ^ y3)) continue;
              if (ithbit(conf, 7) == 1) {
                if ((x0 ^ x2) != (x1 ^ x3)) continue;
                if (ithbit(conf, 6) == 1 && x0 != x2) continue;
                if (ithbit(conf, 6) == 0 && x0 == x2) continue;
                else z = (z << 8) | (x0 ^ x2);
              }
              if (ithbit(conf, 10) == 1) {
                if ((y0 ^ y2) != (y1 ^ y3)) continue;
                if (ithbit(conf, 9) == 1 && y0 != y2) continue;
                if (ithbit(conf, 9) == 0 && y0 == y2) continue;
                else z = (z << 8) | (y0 ^ y2);
              }
              tmp[z] += 1.0;
            }
          }
        }
        for (auto const & p : tmp) p1 = max(p1, p.second);
      }
    }
    else if (ithbit(conf, 7) == 1) {
      for (unsigned d = ((ithbit(conf, 6) == 1) ? 0 : 1); d < ((ithbit(conf, 6) == 1) ? 1 : 256); ++d) {
        map<unsigned, double> tmp;
        for (unsigned x0 = 0; x0 < 256; ++x0) {
          unsigned y0 = sbox[x0];
          unsigned x2 = x0 ^ d;
          unsigned y2 = sbox[x2];
          for (unsigned x1 = 0; x1 < 256; ++x1) {
            unsigned y1 = sbox[x1];
            for (unsigned x3 = 0; x3 < 256; ++x3) {
              unsigned y3 = sbox[x3];
              unsigned z = 0;
              if ((x0 ^ x2) != (x1 ^ x3)) continue;
              if (ithbit(conf, 10) == 1) {
                if ((y0 ^ y2) != (y1 ^ y3)) continue;
                else z = (z << 8) | (y0 ^ y2);
              }
              tmp[z] += 1.0;
            }
          }
        }
        for (auto const & p : tmp) p1 = max(p1, p.second);
      }
    }
    else {
      for (unsigned d = 1; d < 256; ++d) {
        map<unsigned, double> tmp;
        for (unsigned x0 = 0; x0 < 256; ++x0) {
          unsigned y0 = sbox[x0];
          unsigned y2 = y0 ^ d;
          //unsigned x2 = rsbox[y2];
          for (unsigned x1 = 0; x1 < 256; ++x1) {
            unsigned y1 = sbox[x1];
            for (unsigned x3 = 0; x3 < 256; ++x3) {
              unsigned y3 = sbox[x3];
              unsigned z = 0;
              if ((y0 ^ y2) != (y1 ^ y3)) continue;
              tmp[z] += 1.0;
            }
          }
        }
        for (auto const & p : tmp) p1 = max(p1, p.second);
      }
    }
    p1 = p1/nmess;
    if (ithbit(conf, 1) == 0 && ithbit(conf, 2) == 1) {
      if (ithbit(conf, 7) == 0) {
        p1 = p1/256.0;
        if (ithbit(conf, 8) == 1) p1 = p1/256.0;
      }
    }
    else if (ithbit(conf, 7) == 0 && ithbit(conf, 8) == 1) {
      if (ithbit(conf, 1) == 0) p1 = (p1/256.0)/256.0;
      else p1 = p1/256.0;
    }

    if (ithbit(conf, 4) == 0 && ithbit(conf, 5) == 1) {
      p1 = p1/256.0;
    }
    #pragma omp critical
    {
      cpt++;
      for (unsigned i = 0; i < 12; ++i) {
        cout << ithbit(conf, i);
        if (i%3 == 2) cout << " | ";
      }
      cout << " --> " << round(-log2(p1)*10)/10.0 << endl;
      mapproba[conf] = round(-log2(p1)*10)/10.0;
      myproba.emplace(mapproba[conf]);
    }
  }
  map<double, vector<unsigned>> tmp;
  tmp[0] = vector<unsigned> ({0,0,0,0,0});
  tmp[5.4] = vector<unsigned> ({1,0,0,0,0});
  tmp[6] = vector<unsigned> ({0,1,0,0,0});
  tmp[8] = vector<unsigned> ({0,0,0,1,0});
  tmp[12] = vector<unsigned> ({0,1,1,0,0});
  tmp[13.4] = vector<unsigned> ({1,0,0,1,0});
  tmp[14] = vector<unsigned> ({0,1,0,1,0});
  tmp[16] = vector<unsigned> ({0,1,0,0,1});
  tmp[20] = vector<unsigned> ({0,1,1,1,0});
  tmp[21.4] = vector<unsigned> ({1,1,0,0,1});
  tmp[24] = vector<unsigned> ({0,1,0,1,1});
  for (auto const & p : mapproba) {
    auto const & v = tmp[p.second];
    for (unsigned i = 0; i < 5; ++i) {
      if (v[i] == 0) continue;
      cout << "(!p" << i;
      cout << " & " << (ithbit(p.first, 0) == 0 ? "!zXup" : "zXup");
      cout << " & " << (ithbit(p.first, 1) == 0 ? "!kXup" : "kXup");
      cout << " & " << (ithbit(p.first, 2) == 0 ? "!sXup" : "sXup");
      cout << " & " << (ithbit(p.first, 4) == 0 ? "!kSXup" : "kSXup");
      cout << " & " << (ithbit(p.first, 5) == 0 ? "!sSXup" : "sSXup");
      cout << " & " << (ithbit(p.first, 7) == 0 ? "!kXlo" : "kXlo");
      cout << " & " << (ithbit(p.first, 8) == 0 ? "!sXlo" : "sXlo");
      cout << " & " << (ithbit(p.first, 9) == 0 ? "!zSXlo" : "zSXlo");
      cout << " & " << (ithbit(p.first, 10) == 0 ? "!kSXlo" : "kSXlo");
      cout << " & " << (ithbit(p.first, 11) == 0 ? "!sSXlo" : "sSXlo");
      cout << ") | ";
    }
  }
  getchar();
  cout << cpt << endl;
  for (auto p : myproba) cout << p << " ";
  cout << endl;

  return mapproba;
}
