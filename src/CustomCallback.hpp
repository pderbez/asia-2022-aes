#ifndef DEF_BOOMAES
#define DEF_BOOMAES

#include <vector>
#include <map>
#include "/opt/gurobi/linux64/include/gurobi_c++.h"
#include "Matrix.hpp"


class mycallback: public GRBCallback
{
public:
	Matrix mat;
	unsigned numvars;
	GRBVar * varsZup;
	GRBVar * varsKup;
	GRBVar * varsSup;
	GRBVar * varsDup;
	GRBVar * varsSKup;
	std::map<unsigned, unsigned> mapSKup;
	GRBVar * varsZlo;
	GRBVar * varsKlo;
	GRBVar * varsSlo;
	GRBVar * varsDlo;
	GRBVar * varsSKlo;
	std::map<unsigned, unsigned> mapSKlo;

	mycallback(int xnumvars, GRBVar* xvarsZup, GRBVar* xvarsKup, GRBVar* xvarsSup, GRBVar* xvarsDup, GRBVar* xvarsSKup, std::map<unsigned, unsigned> const & xmapSKup, GRBVar* xvarsZlo, GRBVar* xvarsKlo, GRBVar* xvarsSlo, GRBVar* xvarsDlo, GRBVar* xvarsSKlo, std::map<unsigned, unsigned> const & xmapSKlo, Matrix const & eqs) {
		numvars = xnumvars;
		varsZup = xvarsZup;
		varsKup = xvarsKup;
		varsSup = xvarsSup;
		varsDup = xvarsDup;
		varsSKup = xvarsSKup;
		mapSKup = xmapSKup;
		varsZlo = xvarsZlo;
		varsKlo = xvarsKlo;
		varsSlo = xvarsSlo;
		varsDlo = xvarsDlo;
		varsSKlo = xvarsSKlo;
		mapSKlo = xmapSKlo;
		mat = eqs;
	}
protected:
	void callback();
};

#endif
