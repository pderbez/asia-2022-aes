#include <set>
#include <cmath>
#include "CustomCallback.hpp"
#include "SysOfEqs.hpp"

using namespace std;

// unsigned ithbit(unsigned x, unsigned i) {return ((x >> i) & 1);}

void AES_states(GRBModel & model, vector<GRBVar> & sX, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & dX) {
  vector<unsigned> shiftRows ({0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3});
  vector<unsigned> shiftRowsInv (16);
  for (unsigned i = 0; i < 16; ++i) shiftRowsInv[shiftRows[i]] = i;

  unsigned R = ((sX.size()/16) - 3)/4;
  for (unsigned i = 0; i < (4*R+3)*16; ++i) {
    sX[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    kX[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    zX[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    dX[i] = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
  }

  for (unsigned i = 0; i < (4*R+3)*16; ++i) {
    // zero --> known --> set to specific value
    model.addConstr(zX[i] <= kX[i]);
    model.addConstr(kX[i] <= sX[i]);
  }

  for (unsigned r = 0; r <= R; ++r) {
    for (unsigned i = 0; i < 16; ++i) {
      model.addConstr(sX[(4*r+1)*16+i] == 1); // all diff in keys are set to specific values
      model.addConstr(dX[(4*r+1)*16+i] == 1); // keys belong to the distinguisher
    }
  }

  for (unsigned r = 0; r < R; ++r) {
    for (unsigned i = 0; i < 16; ++i) {
      // if dX = 0 <---> dS(X) = 0
      model.addConstr(zX[(4*r+2)*16+i] == zX[(4*r+3)*16+i]);
    }
  }

  for (unsigned r = 0; r <= R; ++r) {
    for (unsigned i = 0; i < 16; ++i) {
      // X_{r+1}[i] + Z_{r}[i] + K_{r+1}[i] = 0
      model.addConstr(sX[(4*r+2)*16 + i] == sX[(4*r+0)*16 + i]);
      model.addConstr(dX[(4*r+2)*16 + i] == dX[(4*r+0)*16 + i]);

      model.addConstr(kX[(4*r+2)*16 + i] - kX[(4*r+1)*16 + i] - kX[(4*r+0)*16 + i] >= -1);
      model.addConstr(kX[(4*r+1)*16 + i] - kX[(4*r+2)*16 + i] - kX[(4*r+0)*16 + i] >= -1);
      model.addConstr(kX[(4*r+0)*16 + i] - kX[(4*r+1)*16 + i] - kX[(4*r+2)*16 + i] >= -1);
      model.addConstr(zX[(4*r+2)*16 + i] - zX[(4*r+1)*16 + i] - zX[(4*r+0)*16 + i] >= -1);
      model.addConstr(zX[(4*r+1)*16 + i] - zX[(4*r+2)*16 + i] - zX[(4*r+0)*16 + i] >= -1);
      model.addConstr(zX[(4*r+0)*16 + i] - zX[(4*r+1)*16 + i] - zX[(4*r+2)*16 + i] >= -1);
    }
  }

  for (unsigned r = 0; r < R-1; ++r) {
    for (unsigned c = 0; c < 4; ++c) {
      {
        GRBLinExpr es = 0;
        for (unsigned i = 0; i < 4; ++i) es += sX[(4*r+3)*16 + shiftRowsInv[4*c + i]] + sX[(4*r+4)*16 + 4*c + i];
        GRBLinExpr ez3 = 0;
        for (unsigned i = 0; i < 4; ++i) ez3 += zX[(4*r+3)*16 + shiftRowsInv[4*c + i]];
        GRBLinExpr ez4 = 0;
        for (unsigned i = 0; i < 4; ++i) ez4 += zX[(4*r+4)*16 + 4*c + i];
        GRBLinExpr ez = ez3 + ez4;
        GRBLinExpr ek3 = 0;
        for (unsigned i = 0; i < 4; ++i) ek3 += kX[(4*r+3)*16 + shiftRowsInv[4*c + i]];
        GRBLinExpr ek4 = 0;
        for (unsigned i = 0; i < 4; ++i) ek4 += kX[(4*r+4)*16 + 4*c + i];
        GRBLinExpr ek = ek3 + ek4;

        {
          auto e = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
          e.set(GRB_IntAttr_BranchPriority, 100);
          model.addConstr(8 - ez <= 8*e);
          model.addConstr(8 - ez >= 5*e);
        }

        {
          auto e = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
          e.set(GRB_IntAttr_BranchPriority, 100);
          model.addConstr(8 - ek <= 8*e);
          model.addConstr(8 - ek >= 5*e);
        }

        // // number of {known, zero} around MixColumns in {0,1,2,3,8}
        // for (unsigned i = 0; i < 4; ++i) {
        //   model.addConstr(ez - 3 <= 5*zX[(4*r+3)*16 + shiftRowsInv[4*c + i]]);
        //   //model.addConstr(ez - 3 <= 5*zX[(4*r+4)*16 + 4*c + i]);
        //   model.addConstr(ek - 3 <= 5*kX[(4*r+3)*16 + shiftRowsInv[4*c + i]]);
        //   //model.addConstr(ek - 3 <= 5*kX[(4*r+4)*16 + 4*c + i]);
        // }
        // model.addConstr(4*(ez3 - 3) <= ez4);
        // model.addConstr(4*(ek3 - 3) <= ek4);

        // specific values must come from the key
        GRBLinExpr ek2 = 0;
        for (unsigned i = 0; i < 4; ++i) ek2 += kX[(4*r+3)*16 + shiftRowsInv[4*c + i]] + kX[(4*r+6)*16 + 4*c + i];
        model.addConstr(ek2 >= 4*(sX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] + sX[(4*r+6)*16 + 4*c + 0] - 1));
      }
    }
  }

  for (unsigned i = 0; i < 16; ++i) { // no MixColumns for the last round
    model.addConstr(sX[(4*R-1)*16 + i] == sX[(4*R)*16 + shiftRows[i]]);
    model.addConstr(kX[(4*R-1)*16 + i] == kX[(4*R)*16 + shiftRows[i]]);
    model.addConstr(zX[(4*R-1)*16 + i] == zX[(4*R)*16 + shiftRows[i]]);
    model.addConstr(dX[(4*R-1)*16 + i] == dX[(4*R)*16 + shiftRows[i]]);
  }
}

void AES_statesUp(GRBModel & model, vector<GRBVar> & sX, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & dX) {
  vector<unsigned> shiftRows ({0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3});
  vector<unsigned> shiftRowsInv (16);
  for (unsigned i = 0; i < 16; ++i) shiftRowsInv[shiftRows[i]] = i;

  unsigned R = ((sX.size()/16) - 3)/4;

  for (unsigned r = 0; r < R; ++r) {
    // Propagation dist. through Sbox
    for (unsigned i = 0; i < 16; ++i) model.addConstr(dX[(4*r + 2)*16 + i] <= dX[(4*r + 3)*16 + i]);
    // Do not take back control
    for (unsigned i = 0; i < 16; ++i) model.addConstr(1-dX[(4*r + 2)*16 + i] + sX[(4*r + 2)*16 + i] + 1-sX[(4*r + 3)*16 + i] >= 1);
    // Set Priority
    for (unsigned i = 0; i < 16; ++i) zX[(4*r + 2)*16 + i].set(GRB_IntAttr_BranchPriority, 100);
  }

  for (unsigned r = 0; r < R-1; ++r) {
    for (unsigned c = 0; c < 4; ++c) {
      GRBLinExpr eout = 0;
      // Propagation dist. through Mixcolumns
      for (unsigned i = 1; i < 4; ++i) model.addConstr(dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] == dX[(4*r+3)*16 + shiftRowsInv[4*c + i]]);
      for (unsigned i = 0; i < 4; ++i) eout += dX[(4*r+4)*16 + 4*c + i];
      model.addConstr(4*dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] <= eout);
      model.addConstr(eout <= dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] + 3);
    }
    for (unsigned c = 0; c < 4; ++c) {
      GRBLinExpr eout = 0, ein = 0;
      // Propagation specific through Mixcolumns
      for (unsigned i = 0; i < 4; ++i) eout += sX[(4*r+4)*16 + 4*c + i];
      for (unsigned i = 0; i < 4; ++i) ein += sX[(4*r+3)*16 + shiftRowsInv[4*c + i]];
      model.addConstr(3*(1-dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]]) + 4*sX[(4*r+4)*16 + 4*c + 0] >= eout);
      model.addConstr(7*(1-dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]]) + ein + eout >= 8*sX[(4*r+4)*16 + 4*c + 0]);
      model.addConstr(3*dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] + 4*sX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] >= ein);
      model.addConstr(7*dX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] + ein + eout >= 8*sX[(4*r+3)*16 + shiftRowsInv[4*c + 0]]);
    }
  }
  for (unsigned r = 0; r <= R; ++r) {
    for (unsigned i = 0; i < 16; ++i) {
      model.addConstr(1 - sX[(4*r + 2)*16 + i] + dX[(4*r + 2)*16 + i] + zX[(4*r + 2)*16 + i] >= 1);
    }
  }
}

void AES_statesLo(GRBModel & model, vector<GRBVar> & sX, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & dX) {
  vector<unsigned> shiftRows ({0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3});
  vector<unsigned> shiftRowsInv (16);
  for (unsigned i = 0; i < 16; ++i) shiftRowsInv[shiftRows[i]] = i;

  unsigned R = ((sX.size()/16) - 3)/4;

  for (unsigned r = 0; r < R; ++r) {
    // Propagation dist. through Sbox
    for (unsigned i = 0; i < 16; ++i) model.addConstr(dX[(4*r + 2)*16 + i] >= dX[(4*r + 3)*16 + i]);
    // Do not take back control
    for (unsigned i = 0; i < 16; ++i) model.addConstr(1-dX[(4*r + 3)*16 + i] + sX[(4*r + 3)*16 + i] + 1-sX[(4*r + 2)*16 + i] >= 1);
    // Set Priority
    for (unsigned i = 0; i < 16; ++i) zX[(4*r + 2)*16 + i].set(GRB_IntAttr_BranchPriority, 100);
  }

  for (unsigned r = 0; r < R-1; ++r) {
    for (unsigned c = 0; c < 4; ++c) {
      GRBLinExpr eout = 0;
      // Propagation dist. through Mixcolumns
      for (unsigned i = 1; i < 4; ++i) model.addConstr(dX[(4*r+4)*16 + 4*c + 0] == dX[(4*r+4)*16 + 4*c + i]);
      for (unsigned i = 0; i < 4; ++i) eout += dX[(4*r+3)*16 + shiftRowsInv[4*c + i]];
      model.addConstr(4*dX[(4*r+4)*16 + 4*c + 0] <= eout);
      model.addConstr(eout <= dX[(4*r+4)*16 + 4*c + 0] + 3);
    }
    for (unsigned c = 0; c < 4; ++c) {
      GRBLinExpr eout = 0, ein = 0;
      // Propagation specific through Mixcolumns
      for (unsigned i = 0; i < 4; ++i) eout += sX[(4*r+3)*16 + shiftRowsInv[4*c + i]];
      for (unsigned i = 0; i < 4; ++i) ein += sX[(4*r+4)*16 + 4*c + i];
      model.addConstr(3*(1-dX[(4*r+4)*16 + 4*c + 0]) + 4*sX[(4*r+3)*16 + shiftRowsInv[4*c + 0]] >= eout);
      model.addConstr(7*(1-dX[(4*r+4)*16 + 4*c + 0]) + ein + eout >= 8*sX[(4*r+3)*16 + shiftRowsInv[4*c + 0]]);
      model.addConstr(3*dX[(4*r+4)*16 + 4*c + 0] + 4*sX[(4*r+4)*16 + 4*c + 0] >= ein);
      model.addConstr(7*dX[(4*r+4)*16 + 4*c + 0] + ein + eout >= 8*sX[(4*r+4)*16 + 4*c + 0]);
    }
  }
  for (unsigned r = 0; r < R; ++r) {
    for (unsigned i = 0; i < 16; ++i) {
      model.addConstr(1 - sX[(4*r + 3)*16 + i] + dX[(4*r + 3)*16 + i] + zX[(4*r + 3)*16 + i] >= 1);
    }
  }
}

void AES256_keys(GRBModel & model, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & kK, map<unsigned, unsigned> & mapSK) {
  unsigned R = ((kX.size()/16) - 3)/4;
  //Keyschedule
  for (unsigned r = 2; r <= R; ++r) {
    for (unsigned i = 4; i < 16; ++i) {
      // K_{r}[i] = K_r[i-4] + K_{r-2}[i]
      model.addConstr(kX[(4*r+1)*16 + i] - kX[(4*r+1)*16 + i-4] - kX[(4*r-7)*16 + i] >= -1);
      model.addConstr(kX[(4*r+1)*16 + i-4] - kX[(4*r+1)*16 + i] - kX[(4*r-7)*16 + i] >= -1);
      model.addConstr(kX[(4*r-7)*16 + i] - kX[(4*r+1)*16 + i-4] - kX[(4*r+1)*16 + i] >= -1);
      model.addConstr(zX[(4*r+1)*16 + i] - zX[(4*r+1)*16 + i-4] - zX[(4*r-7)*16 + i] >= -1);
      model.addConstr(zX[(4*r+1)*16 + i-4] - zX[(4*r+1)*16 + i] - zX[(4*r-7)*16 + i] >= -1);
      model.addConstr(zX[(4*r-7)*16 + i] - zX[(4*r+1)*16 + i-4] - zX[(4*r+1)*16 + i] >= -1);
    }
    for (unsigned i = 0; i < 4; ++i) {
      unsigned j = ((r%2 == 0) ? (i+1)%4 : i) + 12;
      // K_{r}[i] = S(K_{r-1}[j]) + K_{r-2}[i]
      auto numvar = kK.size();
      mapSK[(4*r-3)*16 + j] = numvar;
      auto kSK = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
      kK.emplace_back(kSK);
      auto zSK = zX[(4*r-3)*16 + j];
      // S(K_{r-1}[j]) = 0 implies S(K_{r-1}[j]) known
      model.addConstr(zSK <= kSK);
      model.addConstr(kX[(4*r+1)*16 + i] - kSK - kX[(4*r-7)*16 + i] >= -1);
      model.addConstr(kSK - kX[(4*r+1)*16 + i] - kX[(4*r-7)*16 + i] >= -1);
      model.addConstr(kX[(4*r-7)*16 + i] - kX[(4*r+1)*16 + i] - kSK >= -1);
      model.addConstr(zX[(4*r+1)*16 + i] - zSK - zX[(4*r-7)*16 + i] >= -1);
      model.addConstr(zSK - zX[(4*r+1)*16 + i] - zX[(4*r-7)*16 + i] >= -1);
      model.addConstr(zX[(4*r-7)*16 + i] - zX[(4*r+1)*16 + i] - zSK >= -1);
      // can't control both K_{r-1}[j] and S(K_{r-1}[j]) unless diff is 0
      model.addConstr(zSK - kSK - kX[(4*r-3)*16 + j] >= -1);
    }
  }
}

void AES128_keys(GRBModel & model, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & kK, map<unsigned, unsigned> & mapSK) {
  unsigned R = ((kX.size()/16) - 3)/4;
  //Keyschedule
  for (unsigned r = 1; r <= R; ++r) {
    for (unsigned i = 4; i < 16; ++i) {
      // K_{r}[i] = K_r[i-4] + K_{r-1}[i]
      model.addConstr(kX[(4*r+1)*16 + i] - kX[(4*r+1)*16 + i-4] - kX[(4*r-3)*16 + i] >= -1);
      model.addConstr(kX[(4*r+1)*16 + i-4] - kX[(4*r+1)*16 + i] - kX[(4*r-3)*16 + i] >= -1);
      model.addConstr(kX[(4*r-3)*16 + i] - kX[(4*r+1)*16 + i-4] - kX[(4*r+1)*16 + i] >= -1);
      model.addConstr(zX[(4*r+1)*16 + i] - zX[(4*r+1)*16 + i-4] - zX[(4*r-3)*16 + i] >= -1);
      model.addConstr(zX[(4*r+1)*16 + i-4] - zX[(4*r+1)*16 + i] - zX[(4*r-3)*16 + i] >= -1);
      model.addConstr(zX[(4*r-3)*16 + i] - zX[(4*r+1)*16 + i-4] - zX[(4*r+1)*16 + i] >= -1);
    }
    for (unsigned i = 0; i < 4; ++i) {
      unsigned j = ((i+1)%4) + 12;
      // K_{r}[i] = S(K_{r-1}[j]) + K_{r-1}[i]
      auto numvar = kK.size();
      mapSK[(4*r-3)*16 + j] = numvar;
      auto kSK = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
      kK.emplace_back(kSK);
      auto zSK = zX[(4*r-3)*16 + j];
      // S(K_{r-1}[j]) = 0 implies S(K_{r-1}[j]) known
      model.addConstr(zSK <= kSK);
      model.addConstr(kX[(4*r+1)*16 + i] - kSK - kX[(4*r-3)*16 + i] >= -1);
      model.addConstr(kSK - kX[(4*r+1)*16 + i] - kX[(4*r-3)*16 + i] >= -1);
      model.addConstr(kX[(4*r-3)*16 + i] - kX[(4*r+1)*16 + i] - kSK >= -1);
      model.addConstr(zX[(4*r+1)*16 + i] - zSK - zX[(4*r-3)*16 + i] >= -1);
      model.addConstr(zSK - zX[(4*r+1)*16 + i] - zX[(4*r-3)*16 + i] >= -1);
      model.addConstr(zX[(4*r-3)*16 + i] - zX[(4*r+1)*16 + i] - zSK >= -1);
      // can't control both K_{r-1}[j] and S(K_{r-1}[j]) unless diff is 0
      model.addConstr(zSK - kSK - kX[(4*r-3)*16 + j] >= -1);
    }
  }
}

void AES192_keys(GRBModel & model, vector<GRBVar> & kX, vector<GRBVar> & zX, vector<GRBVar> & kK, map<unsigned, unsigned> & mapSK) {
  unsigned R = ((kX.size()/16) - 3)/4;
  //Keyschedule
  for (unsigned r = 1; r <= R; ++r) {
    for (unsigned c = 0; c < 4; ++c) {
      if (c + 4*r < 6) continue;
      unsigned u0 = (4*r + 1)*16 + 4*c;
      unsigned u1 = (c == 0) ? (4*(r-1) + 1)*16 + 4*3 : u0 - 4;
      unsigned u2 = (c >= 2) ? (4*(r-1) + 1)*16 + 4*(c-2) : (4*(r-2) + 1)*16 + 4*(c+2);
      if ((c + 4*r)%6 == 0) {
        for (unsigned i = 0; i < 4; ++i) {
          unsigned j = ((i+1)%4);
          auto numvar = kK.size();
          mapSK[u1 + j] = numvar;
          auto kSK = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
          kK.emplace_back(kSK);
          auto zSK = zX[u1 + j];
          model.addConstr(zSK <= kSK);
          model.addConstr(kX[u0 + i] - kSK - kX[u2 + i] >= -1);
          model.addConstr(kSK - kX[u0 + i] - kX[u2 + i] >= -1);
          model.addConstr(kX[u2 + i] - kX[u0 + i] - kSK >= -1);
          model.addConstr(zX[u0 + i] - zSK - zX[u2 + i] >= -1);
          model.addConstr(zSK - zX[u0 + i] - zX[u2 + i] >= -1);
          model.addConstr(zX[u2 + i] - zX[u0+ i] - zSK >= -1);
          // can't control both K_{r-1}[j] and S(K_{r-1}[j]) unless diff is 0
          model.addConstr(zSK - kSK - kX[u1 + j] >= -1);
        }
      }
      else {
        for (unsigned i = 0; i < 4; ++i) {
          model.addConstr(kX[u0 + i] - kX[u1 + i] - kX[u2 + i] >= -1);
          model.addConstr(kX[u1 + i] - kX[u2 + i] - kX[u0 + i] >= -1);
          model.addConstr(kX[u2 + i] - kX[u0 + i] - kX[u1 + i] >= -1);
          model.addConstr(zX[u0 + i] - zX[u1 + i] - zX[u2 + i] >= -1);
          model.addConstr(zX[u1 + i] - zX[u2 + i] - zX[u0 + i] >= -1);
          model.addConstr(zX[u2 + i] - zX[u0 + i] - zX[u1 + i] >= -1);
        }
      }
    }
  }
}


void modelAES(unsigned R, unsigned key) {
  vector<unsigned> shiftRows ({0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3});
  vector<unsigned> shiftRowsInv (16);
  for (unsigned i = 0; i < 16; ++i) shiftRowsInv[shiftRows[i]] = i;
  // P --> + K  --> S --> L --> + K
  // P: 0..15
  // K0: 16..31
  // X0 : 32..47
  // Y0 = S(X0) : 48..63
  // Z0 = L(Y0) : 64..79
  // Kr[i] : (4*r+1)*16 + i
  // Xr[i] : (4*r+2)*16 + i
  // Yr[i] : (4*r+3)*16 + i
  // Zr[i] : (4*r+4)*16 + i
  // C = XR

  auto mat = (key == 256 ? AES256eqs(R) : (key == 128 ? AES128eqs(R) : AES192eqs(R)));
  cout << mat << endl;

  GRBEnv env = GRBEnv(true);
  env.set("LogFile", "mip1.log");
  env.start();

  GRBModel model = GRBModel(env);
  //model.set(GRB_IntParam_OutputFlag, 0);

  vector<GRBVar> sXup ((4*R+3)*16); // set
  vector<GRBVar> kXup ((4*R+3)*16); // known
  vector<GRBVar> zXup ((4*R+3)*16); // zero
  vector<GRBVar> dXup ((4*R+3)*16); // dist
  vector<GRBVar> kKup; // known S(K)
  map<unsigned, unsigned> mapSKup;

  AES_states(model, sXup, kXup, zXup, dXup);
  AES_statesUp(model, sXup, kXup, zXup, dXup);
  if (key == 256) AES256_keys(model, kXup, zXup, kKup, mapSKup);
  else if (key == 128) AES128_keys(model, kXup, zXup, kKup, mapSKup);
  else if (key == 192) AES192_keys(model, kXup, zXup, kKup, mapSKup);


  vector<GRBVar> sXlo ((4*R+3)*16);
  vector<GRBVar> kXlo ((4*R+3)*16);
  vector<GRBVar> zXlo ((4*R+3)*16);
  vector<GRBVar> dXlo ((4*R+3)*16);
  vector<GRBVar> kKlo;
  map<unsigned, unsigned> mapSKlo;

  AES_states(model, sXlo, kXlo, zXlo, dXlo);
  AES_statesLo(model, sXlo, kXlo, zXlo, dXlo);
  if (key == 256) AES256_keys(model, kXlo, zXlo, kKlo, mapSKlo);
  else if (key == 128) AES128_keys(model, kXlo, zXlo, kKlo, mapSKlo);
  else if (key == 192) AES192_keys(model, kXlo, zXlo, kKlo, mapSKlo);

  for (auto const & p : mapSKup) {
    model.addConstr(zXup[p.first] + zXlo[p.first] + 1 - kXup[p.first] + 1 - kKlo[p.second] >= 1);
    model.addConstr(zXup[p.first] + zXlo[p.first] + 1 - kXlo[p.first] + 1 - kKup[p.second] >= 1);
  }

  GRBLinExpr pr_dist = 0;
  {
    set<double> allproba;
    for (unsigned r = 0; r < R; ++r) {
      for (unsigned i = 0; i < 16; ++i) {
        auto p0 = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
        auto p1 = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
        auto p2 = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
        auto p3 = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
        auto p4 = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);

        model.addConstr(zXup[(4*r+2)*16 + i] + 1-kXup[(4*r+2)*16 + i] + 1-kXup[(4*r+3)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(zXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + zXlo[(4*r+2)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(zXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + kXlo[(4*r+3)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + zXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(zXup[(4*r+2)*16 + i] + kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(zXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + zXlo[(4*r+2)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p1 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);

        model.addConstr(kXup[(4*r+3)*16 + i] + 1-sXup[(4*r+3)*16 + i] + kXlo[(4*r+3)*16 + i] + p4 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+3)*16 + i] + 1-sXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p4 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + kXlo[(4*r+2)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p4 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + sXup[(4*r+3)*16 + i] + 2*(kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p4) >= 2*(dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1));
        //model.addConstr(sXup[(4*r+3)*16 + i] + kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p4 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + sXlo[(4*r+2)*16 + i] + p4 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);

        model.addConstr(kXup[(4*r+3)*16 + i] + 1-sXup[(4*r+3)*16 + i] + 1-kXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(1-kXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + sXup[(4*r+3)*16 + i] + sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(sXup[(4*r+3)*16 + i] + kXlo[(4*r+3)*16 + i] + sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(1-kXup[(4*r+2)*16 + i] + sXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(1-kXup[(4*r+2)*16 + i] + kXup[(4*r+3)*16 + i] + 1-sXup[(4*r+3)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + sXlo[(4*r+2)*16 + i] + p3  >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(1-kXup[(4*r+2)*16 + i] + 1-sXup[(4*r+3)*16 + i] + kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(1-sXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + kXlo[(4*r+3)*16 + i] + 1-sXlo[(4*r+2)*16 + i] + p3 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);

        model.addConstr(1-kXup[(4*r+2)*16 + i] + kXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + p0 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(kXup[(4*r+2)*16 + i] + 1-kXup[(4*r+3)*16 + i] + 1-kXlo[(4*r+2)*16 + i] + kXlo[(4*r+3)*16 + i] + p0 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);

        model.addConstr(kXup[(4*r+2)*16 + i] + kXup[(4*r+3)*16 + i] + zXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+2)*16 + i] + 1-kXlo[(4*r+3)*16 + i] + p2 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);
        model.addConstr(zXup[(4*r+2)*16 + i] + 1-kXup[(4*r+2)*16 + i] + 1-kXup[(4*r+3)*16 + i] + kXlo[(4*r+2)*16 + i] + kXlo[(4*r+3)*16 + i] + p2 >= dXup[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] - 1);

        GRBLinExpr pr_tmp = 5.4*p0 + 6*p1 + 6*p2 + 8*p3 + 10*p4;
        pr_dist += pr_tmp;
      }
    }
    {
      GRBLinExpr obj = pr_dist;
      for (unsigned i = 0; i < 16; ++i) obj += -4*kXup[i] -4*kXlo[(4*R+2)*16 + i] -4*sXup[i] -4*sXlo[(4*R+2)*16 + i];
      model.setObjective(obj, GRB_MINIMIZE);
    }
    model.addConstr(pr_dist <= 127);
  }


  // no variable free for both trails
  for (unsigned r = 0; r <= R; ++r) {
    if (r < R) {
      for (unsigned i = 0; i < 16; ++i) {
        model.addConstr(1-dXup[(4*r+3)*16 + i] + dXup[(4*r+2)*16 + i] + 1-zXup[(4*r+2)*16 + i] >= 1);
        model.addConstr(1-dXlo[(4*r+2)*16 + i] + dXlo[(4*r+3)*16 + i] + 1-zXlo[(4*r+2)*16 + i] >= 1);
        model.addConstr(dXlo[(4*r+3)*16 + i] + dXup[(4*r+2)*16 + i] >= 1);
        model.addConstr(sXlo[(4*r+3)*16 + i] + sXup[(4*r+2)*16 + i] >= dXlo[(4*r+3)*16 + i] + dXup[(4*r+2)*16 + i] - 1);
        //model.addConstr(1-sXlo[(4*r+3)*16 + i] + 1-sXup[(4*r+3)*16 + i] + zXlo[(4*r+3)*16 + i] + zXup[(4*r+3)*16 + i] >= 1);
      }
    }
  }

  for (unsigned i = 0; i < 16; ++i) model.addConstr(dXup[(4*2+3)*16 + i] == 1);
  for (unsigned i = 0; i < 16; ++i) model.addConstr(dXlo[(4*(R-2)+2)*16 + i] == 1);



  // at least one non-zero difference
  {
    GRBLinExpr e = 0;
    for (unsigned i = 0; i < 16; ++i) e += zXup[i] + zXup[i+16] + zXup[5*16 + i];
    model.addConstr(e <= 3*16 - 1);
  }
  {
    GRBLinExpr e = 0;
    for (unsigned i = 0; i < 16; ++i) e += zXlo[i] + zXlo[i+16] + zXlo[5*16 + i];
    model.addConstr(e <= 3*16 - 1);
  }

  // limit guesses
  {
    GRBLinExpr kP = 0;
    for (unsigned i = 0; i < 16; ++i) kP += kXup[i+32];
    GRBLinExpr kC = 0;
    for (unsigned i = 0; i < 16; ++i) kC += kXlo[(4*(R-1)+3)*16 + i];
    GRBVar e = model.addVar(0.0, 1.0, 0.0, GRB_BINARY);
    model.addConstr(pr_dist + 3 - 8*kP <= 130*e);
    model.addConstr(pr_dist + 3 - 8*kC <= 130*(1-e));
  }

  vector<GRBConstr> tmpconstr;
  // if (key == 256)
  // {
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXup[(4*1 + 4)*16 + i] == 1));
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXup[(4*3 + 4)*16 + i] == 1));
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXup[(4*5 + 4)*16 + i] == 1));
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXup[(4*7 + 4)*16 + i] == 1));
  //
  //   {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*6 + 1)*16 + i];} tmpconstr.emplace_back(model.addConstr(e == 15));}
  //
  //   {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*0 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e == 9));}
  //   {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*2 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e == 14));}
  //   {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*4 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e == 14));}
  //
  //   tmpconstr.emplace_back(model.addConstr(zXup[(4*6 + 1)*16 + 5] == 0));
  //
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXlo[(4*13 + 4)*16 + i] == 1));
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXlo[(4*11 + 4)*16 + i] == 1));
  //   for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(zXlo[(4*9 + 4)*16 + i] == 1));
  //   //for (unsigned i = 0; i < 16; ++i) model.addConstr(zXlo[(4*7 + 4)*16 + i] == 1);
  //
  //   {GRBLinExpr e = 0; for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*12 + 1)*16 + i]; tmpconstr.emplace_back(model.addConstr(e == 15));}
  //   tmpconstr.emplace_back(model.addConstr(zXlo[(4*12 + 1)*16 + 0] == 0));
  //
  // }
  if (key == 192) {
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*1 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 15));}
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*2 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 14));}
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*3 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 15));}
    //{GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*4 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 16));}

    //{GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXup[(4*4 + 3)*16 + i] + zXup[(4*3 + 3)*16 + i] + zXup[(4*2 + 3)*16 + i] + zXup[(4*1 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 60));}

    //{GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*7 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 15));}
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*8 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 14));}
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*9 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 15));}
    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*10 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 16));}

    {GRBLinExpr e = 0; {for (unsigned i = 0; i < 16; ++i) e += zXlo[(4*10 + 3)*16 + i] + zXlo[(4*9 + 3)*16 + i] + zXlo[(4*8 + 3)*16 + i] + zXlo[(4*7 + 3)*16 + i];} tmpconstr.emplace_back(model.addConstr(e >= 60));}
  }

  {
    for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(sXup[(4*5 + 2)*16 + i] == 1));
    for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(sXlo[(4*7 + 3)*16 + i] == 1));
  }

  {
    for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(sXup[(4*8 + 2)*16 + i] == 0));
    for (unsigned i = 0; i < 16; ++i) tmpconstr.emplace_back(model.addConstr(sXlo[(4*3 + 3)*16 + i] == 0));
  }


   vector<double> X ((4*R + 3)*16, 0.0);
   vector<double> SK (kKup.size(), 0.0);


  mycallback cb ((4*R+3)*16, zXup.data(), kXup.data(), sXup.data(), dXup.data(), kKup.data(), mapSKup, zXlo.data(), kXlo.data(), sXlo.data(), dXlo.data(), kKlo.data(), mapSKlo, mat);
  model.setCallback(&cb);
  model.set(GRB_IntParam_LazyConstraints , 1);
  model.set(GRB_IntParam_Cuts , 3);
  model.read("tune.prm");


  model.optimize();

  cout << "--- final ---" << endl;
  for (unsigned r = 0; r < 4*R + 3; ++r) {
    cout << ((r%4 == 0) ? 4 : r%4)  << ": ";
    for (unsigned i = 0; i < 16; ++i) {
      if (zXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "0 ";
      else if (dXup[16*r + i].get(GRB_DoubleAttr_X) < 0.5) cout << "* ";
      else if (kXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "1 ";
      else if (sXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "2 ";
      else cout << "* ";
    }
    cout << "    |    ";
    for (unsigned i = 0; i < 16; ++i) {
      if (zXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "0 ";
      else if (dXlo[16*r + i].get(GRB_DoubleAttr_X) < 0.5) cout << "* ";
      else if (kXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "1 ";
      else if (sXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "2 ";
      else cout << "* ";
    }
    cout << endl;
  }

  getchar();

  model.update();


  for (auto & c : tmpconstr) model.remove(c);

  model.optimize();

  cout << "--- final ---" << endl;
  for (unsigned r = 0; r < 4*R + 3; ++r) {
    cout << ((r%4 == 0) ? 4 : r%4)  << ": ";
    for (unsigned i = 0; i < 16; ++i) {
      if (zXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "0 ";
      else if (dXup[16*r + i].get(GRB_DoubleAttr_X) < 0.5) cout << "* ";
      else if (kXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "1 ";
      else if (sXup[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "2 ";
      else cout << "* ";
    }
    cout << "    |    ";
    for (unsigned i = 0; i < 16; ++i) {
      if (zXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "0 ";
      else if (dXlo[16*r + i].get(GRB_DoubleAttr_X) < 0.5) cout << "* ";
      else if (kXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "1 ";
      else if (sXlo[16*r + i].get(GRB_DoubleAttr_X) > 0.5) cout << "2 ";
      else cout << "* ";
    }
    cout << endl;
  }
}
